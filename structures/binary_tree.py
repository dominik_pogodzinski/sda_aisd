class Node(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

    def insert(self, liczba):
        return None

    def search(self, poszukiwana):
        return None

    def print_tree_pre_order(self):
        return None

    def print_tree_in_order(self):
        return None

    def print_tree_post_order(self):
        return None


if __name__ == "__main__":
    root = Node(13)
    root.insert(15)
    root.insert(4)
    root.insert(5)
    print(root.search(3))
    print(root.search(4))
    root.print_tree_post_order()

